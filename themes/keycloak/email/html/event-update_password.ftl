<html>
	<body style="width:100%;max-width:480px;margin:0 auto">
		<div style="padding:10px;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:20px;text-align:left;">
			<img alt="banner" style="width:100%;text-align:center;" src="https://storage.googleapis.com/is-build-assets/keycloak/mp/img/banner.jpg" />
			${kcSanitize(msg("eventUpdatePasswordBodyHtml"))?no_esc}
			<div style="width:100%;padding: 10px;border-top:25px solid #dbdbdb;border-bottom:8px solid #6abb97;background-color:#f0f0f0;font-size:11px;line-height:14px;">
				${kcSanitize(msg("passwordFooterHtml"))?no_esc}
			</div>
		</div>
	</body>
</html>
