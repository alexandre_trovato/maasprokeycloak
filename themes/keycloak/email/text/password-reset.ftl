<#ftl output_format="plainText">
${msg("passwordResetBody",link, linkExpiration, realmName, linkExpirationFormatter(linkExpiration), user.getFirstName())}