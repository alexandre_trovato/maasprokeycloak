<div class="Validator">
    <span id="js-passwords-not-equals" class="Validator-feedbackPassword">${msg("passwordNotEquals")}</span>
  	<div class="Validator-feedbacks">
        <span class="Validator-feedback">
            <span id="js-length-not-valid" class="Validator-feedbackIcon--notValid">
                <div class="Validator-circle"></div>
            </span>
            <span id="js-length-valid" class="Validator-feedbackIcon--valid">
                <svg width="12" height="12">
                    <polyline class="Validator-check" points="1,7 5,11 11,1" fill="none" stroke-width="2px" stroke-linecap="round"></polyline>
                </svg>
            </span>
            <span id="js-length-text" class="Validator-feedbackText">${msg("validatorLength")}</span>
        </span> 
        <span class="Validator-feedback">
            <span id="js-uppercase-not-valid" class="Validator-feedbackIcon--notValid">
                <div class="Validator-circle"></div>
            </span>
            <span id="js-uppercase-valid" class="Validator-feedbackIcon--valid">
                <svg width="12" height="12">
                    <polyline class="Validator-check" points="1,7 5,11 11,1" fill="none" stroke-width="2px" stroke-linecap="round"></polyline>
                </svg>
            </span>
            <span id="js-uppercase-text" class="Validator-feedbackText">${msg("validatorUppercase")}</span>
        </span> 
        <span class="Validator-feedback">
            <span id="js-lowercase-not-valid" class="Validator-feedbackIcon--notValid">
                <div class="Validator-circle"></div>
            </span>
            <span id="js-lowercase-valid" class="Validator-feedbackIcon--valid">
                <svg width="12" height="12">
                    <polyline class="Validator-check" points="1,7 5,11 11,1" fill="none" stroke-width="2px" stroke-linecap="round"></polyline>
                </svg>
            </span>
            <span id="js-lowercase-text" class="Validator-feedbackText">${msg("validatorLowercase")}</span>
        </span> 
        <span class="Validator-feedback">
            <span id="js-number-not-valid" class="Validator-feedbackIcon--notValid">
                <div class="Validator-circle"></div>
            </span>
            <span id="js-number-valid" class="Validator-feedbackIcon--valid">
                <svg width="12" height="12">
                    <polyline class="Validator-check" points="1,7 5,11 11,1" fill="none" stroke-width="2px" stroke-linecap="round"></polyline>
                </svg>
            </span>
            <span id="js-number-text" class="Validator-feedbackText">${msg("validatorNumber")}</span>
        </span> 
        <span class="Validator-feedback">
            <span id="js-char-not-valid" class="Validator-feedbackIcon--notValid">
                <div class="Validator-circle"></div>
            </span>
            <span id="js-char-valid" class="Validator-feedbackIcon--valid">
                <svg width="12" height="12">
                    <polyline class="Validator-check" points="1,7 5,11 11,1" fill="none" stroke-width="2px" stroke-linecap="round"></polyline>
                </svg>
            </span>
            <span id="js-char-text" class="Validator-feedbackText">${msg("validatorChar")}</span>
        </span>  
    </div>
</div>