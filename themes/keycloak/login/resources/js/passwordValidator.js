// Handler when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", function(){
  // Variables
  let passwordNew = ''
  let passswordConfirm = ''

  // Input listeners
  document.addEventListener('keyup', function(event) {
    if (event.target && event.target.id === 'password-new') {
      passwordNew = event.target.value
      checkPasswords()
      checkLength(passwordNew)
      checkUppercase(passwordNew)
      checkLowerCase(passwordNew)
      checkNumber(passwordNew)
      checkSpecialChar(passwordNew)
    }
  })

  document.addEventListener('keyup', function(event) {
    if (event.target && event.target.id === 'password-confirm') {
      passswordConfirm = event.target.value
      checkPasswords()
    }
  })

  // Check input length
  let checkLength = function (value) {
    displayHideIcons(value.length > 8, 'length')
  }

  // Check input has one upper case
  let checkUppercase = function (value) {
    displayHideIcons(/[A-Z]/u.test(value), 'uppercase')
  }

  // Check input has one lower case
  let checkLowerCase = function (value) {
    displayHideIcons(/[a-z]/u.test(value), 'lowercase')
  }

  // Check input has one number
  let checkNumber = function (value) {
    displayHideIcons(/[0-9]/.test(value), 'number')
  }

  // Check input has one special char
  let checkSpecialChar = function (value) {
    displayHideIcons(/[-!@$#$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/.test(value), 'char')
  }

  // Check if passwords are equals
  let checkPasswords = function () {
    if (passwordNew !== '' && passswordConfirm !== '' && passwordNew !== passswordConfirm) {
      document.querySelector('#js-passwords-not-equals').style.visibility = 'visible'
      document.querySelector('#js-passwords-not-equals').style.height = 'auto'
      document.querySelector('#js-passwords-not-equals').style.opacity = '1'
    } else {
      document.querySelector('#js-passwords-not-equals').style.visibility = 'hidden'
      document.querySelector('#js-passwords-not-equals').style.height = '0'
      document.querySelector('#js-passwords-not-equals').style.opacity = '0'
    }
  }

  // Display or hide icons regarding of status
  let displayHideIcons = function (value, itemClass) {
    if (value) {
      document.querySelector('#js-' + itemClass + '-not-valid').style.display = 'none'
      document.querySelector('#js-' + itemClass + '-valid').style.display = 'inline-block'
      document.querySelector('#js-' + itemClass + '-text').classList.add('Validator-feedbackText--valid')
    } else {
      document.querySelector('#js-' + itemClass + '-valid').style.display = 'none'
      document.querySelector('#js-' + itemClass + '-not-valid').style.display = 'inline-block'
      document.querySelector('#js-' + itemClass + '-text').classList.remove('Validator-feedbackText--valid')
    }
  }    
})