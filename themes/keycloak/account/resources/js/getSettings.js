// Handler when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", function() {
	let urlParameters = window.location.href.split('/')
	let realm = ''
	let env = 'dev'
	let settings = {}
	
	for (let i = 0; i < urlParameters.length; i++) {
		// Get realm name
		if (urlParameters[i] === 'realms') {
			realm = urlParameters[i + 1]
		}
		// Get env
		if (urlParameters[i].indexOf('.sso.') !== -1) {
			let infos = urlParameters[i].split('.')

			for (let j = 0; j < infos.length; j++) {
				if (infos[j] === 'sso') {
					env = infos[j + 1]
				}
			}
		}
	}

    // Only if realm is not master
    if (realm !== 'master') {
        // Get settings from API
        fetch('https://' + env + '.instant-system.com/maaspro/v1/networks/64/realms/settings?realm=' + realm)
            .then(response => response.json())
            .then(data => {
                // Check if profiles are defined
                let settings = data.profiles ? data.profiles[0].settings : data[0].settings
                // Set primary color on custom elements
                for (let i = 0; i < settings.length; i++) {
                    if (settings[i].name === 'primary_color') {
                        let elements = document.getElementsByClassName('js-custom-color')
                        for (let j = 0; j < elements.length; j++) {
                            // Texts
                            if (elements[j].tagName === 'DIV') {
                                elements[j].style.color = settings[i].value
                            }
                            // Buttons
                            if (elements[j].tagName === 'INPUT' || elements[j].tagName === 'BUTTON') {
                                elements[j].style.backgroundColor  = settings[i].value
                            }
                        }
                    }
                }
            })
    }
})
